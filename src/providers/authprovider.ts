import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Rx';

/*
  Generated class for the Authprovider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Authprovider {

  baseUrl = "http://localhost:3000/";

  constructor(public http: Http) {

  }

  private setHeader() : Headers {
    let headerConfig = {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    }

    return new Headers(headerConfig)

  }

  register(path: string, data): Observable<any> {
    return this.http.post(`${this.baseUrl}${path}`, JSON.stringify(data), {headers: this.setHeader( )})
      .catch(this.formatError)
      .map((res:Response)=> res.json());
  }

  formatError(error) {
    return Observable.throw(error.json());
  }

  login(path:string, body: Object = {}): Observable<any> {
    // POST patient details to login API
    return this.http.post(`${this.baseUrl}${path}`, JSON.stringify(body), {headers: this.setHeader( )})
      .map((res:Response)=> {
        // If status is not 200, throw error else send response
        if (res.status < 200 || res.status >= 300) {
          throw new Error("Something went wrong" + res.status);
        } else {
          return res.json();
        }
      });
  }

  forgot_password(path:string, phone): Observable<any> {
    // POST doctor details to login API
    return this.http.get(`${this.baseUrl}${path}?&phone_number=${phone}`, {headers: this.setHeader( )})
      .map((res:Response)=> {
        // If status is not 200, throw error else send response
        if (res.status < 200 || res.status >= 300) {
          throw new Error("Something went wrong" + res.status);
        } else {
          return res.json();
        }
      });
  }

}
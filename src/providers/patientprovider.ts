import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class Patientprovider {

  baseUrl = "http://localhost:3000/";

  constructor(public http: Http) {
    console.log('Hello Patientprovider Provider');
  }

  private setHeader(token: string) : Headers {
    let headerConfig = {
      Authorization : `Bearer ${token}`,
      'Content-Type': 'application/json'
    }

    return new Headers(headerConfig);

  }

  updateProfile(path: string, token: string, body) {
    return this.http.put(`${this.baseUrl}${path}`, JSON.stringify(body), {headers: this.setHeader( token )}).map((res:Response) => {
        // If status is not 200, throw error else send response
        if (res.status < 200 || res.status >= 300) {
          throw new Error("Something went wrong" + res.status);
        }
    });
  }

}
import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { Login } from './../pages/login/login';
import { HomePage } from './../pages/home/home';
import { TokenManager } from './../providers/token-manager';



@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any;


  constructor(platform: Platform,
              statusBar: StatusBar,
              splashScreen: SplashScreen,
              private tokenManager: TokenManager) {

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      this.check_authentication();
    });
  }

  check_authentication() {
    // let token = this.tokenManager.check_token();
    this.tokenManager.check_token().subscribe((response) => {
      if(response) {
        this.rootPage = HomePage;
      } else {
        this.rootPage = Login;
      }
    });
  }
}


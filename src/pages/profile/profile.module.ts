import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Profile } from './profile';
import { Patientprovider } from "../../providers/patientprovider";

@NgModule({
  declarations: [
    Profile,
  ],
  imports: [
    IonicPageModule.forChild(Profile),
  ],
  providers: [
    Patientprovider
  ]
})
export class ProfileModule {}

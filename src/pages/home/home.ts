import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { Login } from './../login/login';
import { Profile } from './../profile/profile';
import { Chat } from './../chat/chat';
import { Patientprovider } from './../../providers/patientprovider';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  profile: any;
  chat: any;
  profile_path: string = "doctors/details";
  // userDetails: Object = {auth_token: '', number};

  constructor(public navCtrl: NavController,
              private pp: Patientprovider) {
    this.profile = Profile;
    this.chat = Chat;
  }

  logout() {
    if(localStorage.getItem('auth_token')) {
      localStorage.removeItem('auth_token');
      this.navCtrl.setRoot(Login);
      this.navCtrl.popToRoot();
    }
  }

  show_chats() {
    this.navCtrl.push(this.chat);
  }

  show_profile() {

    let token = localStorage.getItem('auth_token');
    console.log(token);
    // this.pp.getProfile(this.profile_path, token ).subscribe((res) => {
    //   this.navCtrl.push(this.profile, res);
    // });

  }


}

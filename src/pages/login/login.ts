import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { HomePage } from './../home/home';
import { Register } from './../register/register';
import { Authprovider } from './../../providers/authprovider';
import { TokenManager } from './../../providers/token-manager';

import { ModalController } from 'ionic-angular';
import { ForgotPassword } from './../forgot-password/forgot-password';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class Login {

  register: any;
  home: any;
  login_path: string = "login";
  signin_form: FormGroup;

  user = {
    patient: {
      'phone_number' : null,
      'password' : null
    }
  };

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public modalCtrl: ModalController,
              private fb: FormBuilder,
              private authProvider: Authprovider,
              private loadingCtrl: LoadingController,
              private alertCtrl: AlertController,
              private tokenManager: TokenManager
              ) {
    this.register = Register;

    this.home = HomePage;

    this.signin_form = fb.group({
      phone_number: ['9767049452', Validators.compose([Validators.required, Validators.pattern(/([0-9]{10})/g)])],
      password: ['Tester123', Validators.required]
    });

  }

get_data(form) {

    // Format user inputs
    this.user.patient["phone_number"] = form.phone_number;
    this.user.patient["password"] = form.password;

    this.loginUser(this.login_path, this.user);
  }

  loginUser(path, user) {
    //Set the please wait loader
    let loader = this.loadingCtrl.create({
      content: "Please wait..."
    });
    loader.present();

    this.authProvider.login(path, user).subscribe((response) => {
      //Dismiss please wait Loader
      loader.dismiss();

      // Login User logic
      this.tokenManager.store_token(response.auth_token);
      this.check_authentication();

    },
    (err) => {
      //Dismiss please wait Loader
      loader.dismiss();
      // Display error message on successful registration
      let errors = JSON.parse(err._body).errors;
      let alert = this.alertCtrl.create({
        title: "Error",
        message: errors.patient[0],
        buttons: ["ok"]});
      alert.present();
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Login');
  }

  check_authentication() {
    this.tokenManager.check_token().subscribe((response) => {
      if(response) {
        this.navCtrl.setRoot(HomePage);
      } else {
        this.navCtrl.setRoot(Login);
      }
      this.navCtrl.popToRoot();
    });

  }

  presentModal() {
    let modal = this.modalCtrl.create(ForgotPassword);
    modal.present();
  }

}

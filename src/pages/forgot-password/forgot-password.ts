import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ViewController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Authprovider } from './../../providers/authprovider';

@IonicPage()
@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html',
})
export class ForgotPassword {
  forgot_password_form: FormGroup;
  path: string = "forgot_patients_password";

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private fb: FormBuilder,
              public viewCtrl: ViewController,
              public alertCtrl: AlertController,
              private authProvider: Authprovider) {

    this.forgot_password_form = fb.group({
      phone_number: [null, Validators.required]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgotPassword');
  }

  close() {
    this.viewCtrl.dismiss();
  }

  reset_password(data) {
    console.log(data);
    this.authProvider.forgot_password(this.path, data.phone_number).subscribe((response) => {

      if(response.success) {
        let alert = this.alertCtrl.create({
          title: "Success",
          message: "Please enter OTP",
          buttons: ["ok"]});
        alert.present();
      } else {
        let alert = this.alertCtrl.create({
          title: "Error",
          message: response.error,
          buttons: ["ok"]});
        alert.present();
      }
    });
  }


}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Authprovider } from './../../providers/authprovider';

/**
 * Generated class for the Register page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class Register {

  signup_form: FormGroup;
  email_regex = /^(?:(?:[\w`~!#$%^&*\-=+;:{}'|,?\/]+(?:(?:\.(?:"(?:\\?[\w`~!#$%^&*\-=+;:{}'|,?\/\.()<>\[\] @]|\\"|\\\\)*"|[\w`~!#$%^&*\-=+;:{}'|,?\/]+))*\.[\w`~!#$%^&*\-=+;:{}'|,?\/]+)?)|(?:"(?:\\?[\w`~!#$%^&*\-=+;:{}'|,?\/\.()<>\[\] @]|\\"|\\\\)+"))@(?:[a-zA-Z\d\-]+(?:\.[a-zA-Z\d\-]+)*|\[\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\])$/

register_path: string = "register";

  user = {
    patient : {
      'email' : null,
      'password' : null,
      'phone_number' : null
      }
    };


  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private fb: FormBuilder,
              private authProvider: Authprovider,
              private alertCtrl: AlertController,
              private loadingCtrl: LoadingController) {

    this.signup_form = fb.group({
      // This will validate the input fields
      email: [null, Validators.compose([Validators.required, Validators.pattern(this.email_regex)])],
      password: [null, Validators.compose([Validators.required, Validators.minLength(8)])],
      confirm_password: [null, Validators.compose([Validators.required, Validators.minLength(8)])],
      phone_number: [null, Validators.required]
    });
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad Register');
  }

  registerUser(path, user) {
    //Set the please wait loader
    let loader = this.loadingCtrl.create({
      content: "Please wait..."
    });
    loader.present();

    this.authProvider.register(path, user).subscribe((response) => {
      //Dismiss please wait Loader
      loader.dismiss();

      // Display success message on successful registration
      let alert = this.alertCtrl.create({
          title: "Account created",
          message: "Click ok to login.",
          buttons: [{text: "ok", handler: ()=> {
            this.goBacktoPreviousPage();
          }}]
        });
      alert.present();
    },
    (err) => {
      //Dismiss please wait Loader
      loader.dismiss();

      // Display error message on successful registration
      let alert = this.alertCtrl.create({title: "Error", buttons: ["ok"]});
      alert.present();
    });
  }

  get_data(form) {
    // Format user inputs
    this.user.patient["email"] = form.email;
    this.user.patient["password"] = form.password;
    this.user.patient['phone_number'] = form.phone_number;

    this.registerUser(this.register_path, this.user);

  }

// To take the user back to sign in screen
  goBacktoPreviousPage() {
    this.navCtrl.pop();
  }

}